function __createOrUpdateVideo(
  context,
  contextWidth = context.getAttribute("component_width"),
  contextHeight = context.getAttribute("component_height")
) {
  const mainDivWebComponent =
    context.shadowRoot.getElementById("mainDiv_WebComp");
  mainDivWebComponent.style.width = `${
    contextWidth || context.default_width
  }px`;
  // mainDivWebComponent.style.height = `${
  //   contextHeight || context.default_height
  // }px`;

  // If Already Created then update the widht and Height
  const mainDivSelfieComp =
    context.shadowRoot.getElementById("mainDiv_SelfieComp");

  const videoDiv = mainDivSelfieComp.querySelector("#selfie-video");
  const canvasDiv = mainDivSelfieComp.querySelector("#selfie-canvas");

  if(context.__drawImage_Inteval){
    clearInterval(context.__drawImage_Inteval)
  }

  if (!videoDiv) {
    // Create Video Tag
    const video_element = document.createElement("video");
    video_element.setAttribute("id", "selfie-video");
    video_element.setAttribute("autoplay", true);
    video_element.setAttribute("muted", true);
    video_element.setAttribute("playsinline", true);
    video_element.style.transform = "rotateY(180deg)";
    video_element.style.position = "absolute"
    video_element.style.left = "0px"
    video_element.style.top = "0px"
    // video_element.style.display = "none";

    // Create Canvas Tag
    const canvas_element = document.createElement("canvas");
    canvas_element.setAttribute("id", "selfie-canvas");
    canvas_element.style.position = "absolute"
    canvas_element.style.left = "0px"
    canvas_element.style.top = "0px"

    // Setting Video Width and Height  & Canvas Width and Height
    video_element.width = context.default_width;
    video_element.height = context.default_canvas_height;

    canvas_element.width = context.default_width;
    canvas_element.height = context.default_canvas_height

    // Append Video and Canvas
    mainDivSelfieComp.appendChild(video_element);
    mainDivSelfieComp.appendChild(canvas_element);

    // Start Camera
    // startCamera(context, video_element, canvas_element);

    // Event Listener on Playing the Video
    video_element.addEventListener("play", () => {
      const videoElement = context.shadowRoot.getElementById("selfie-video");
      const canvasElement = context.shadowRoot.getElementById("selfie-canvas");

      context.__drawImage_Inteval = setInterval(() => {
        __drawImage(context, videoElement, canvasElement);
        console.log("Capturing");
      }, 100);
    });
  } else {
    // Setting Video and Canvas Dimensions
    videoDiv.width = `${contextWidth}`;
    videoDiv.height = `${contextHeight - context.default_capture_height}`;

    canvasDiv.width = `${contextWidth}`;
    // canvasDiv.height = `${contextHeight - context.default_capture_height}`;

    // Start Camera
    startCamera(context, videoDiv, canvasDiv);
  }
}

function __createCaptureSection(context) {
  // Section 1---- Creating Section for the Camera
  const maniDivControls = context.shadowRoot.querySelector("#mainDiv_controls");
  // Styling mainDivControls
  maniDivControls.style.height = `${context.default_capture_height}px`;
  maniDivControls.style.width = "100%";
  // maniDivControls.style.backgroundColor = "";
  maniDivControls.style.display = "flex";
  maniDivControls.style.justifyContent = "center";
  maniDivControls.style.alignItems = "center";
  maniDivControls.style.marginTop = "-4px";

  // Positioning Absolute
  maniDivControls.style.position = "absolute"
  maniDivControls.style.top = `${context.default_canvas_height}px`
  
  
  const cameraDiv = document.createElement("div");
  cameraDiv.style.backgroundColor = "white";
  cameraDiv.style.width = "60px";
  cameraDiv.style.height = "60px";
  cameraDiv.style.display = "flex";
  cameraDiv.style.justifyContent = "center";
  cameraDiv.style.alignItems = "center";
  cameraDiv.style.borderRadius = "50%";
  cameraDiv.style.cursor = "pointer";
  
  // Image Tag
  const imageTag = document.createElement("img");
  imageTag.src = "./assets/1_1.png";
  imageTag.width = "35";
  imageTag.height = "35";
  imageTag.setAttribute("alt", "camera");
  
  // Appending Image Tag
  cameraDiv.appendChild(imageTag);
  
  // Appending Camera Div to the Main Controls Div
  maniDivControls.appendChild(cameraDiv);
  
  // Section 2 ---- Creating Section for the Replace Button and Next Button
  const mainDivConfirm = context.shadowRoot.querySelector(
    "#mainDiv_ConfirmSection"
    );
    mainDivConfirm.style.height = `${context.default_capture_height}px`;
    mainDivConfirm.style.width = "100%";
    mainDivConfirm.style.backgroundColor = "black";
    mainDivConfirm.style.display = "flex";
    mainDivConfirm.style.justifyContent = "center";
    mainDivConfirm.style.alignItems = "center";
    mainDivConfirm.style.display = "none"; //Hiding the Div when the photo is getting clicked
    mainDivConfirm.style.marginTop = "-4px";

      // Positioning Absolute
      mainDivConfirm.style.position = "absolute"
      mainDivConfirm.style.top = `${context.default_canvas_height}px`

  const buttonDiv = document.createElement("div");
  buttonDiv.setAttribute("id", "buttonDiv");
  buttonDiv.style.width = "80%";
  buttonDiv.style.display = "flex";
  buttonDiv.style.justifyContent = "space-between";
  // Button  Replace
  const buttonReplace = document.createElement("button");
  buttonReplace.setAttribute("id", "replaceBtn");
  buttonReplace.innerText = "Replace";
  buttonReplace.style.border = "none";
  buttonReplace.style.background = "white";
  buttonReplace.style.padding = "10px 30px";
  buttonReplace.style.borderRadius = "20px";
  buttonReplace.style.fontWeight = "bold";

  // Button Next
  const buttonNext = document.createElement("button");
  buttonNext.setAttribute("id", "nextBtn");
  buttonNext.innerText = "Next";
  buttonNext.style.border = "none";
  buttonNext.style.background = context.mainThemeColor;
  buttonNext.style.padding = "10px 30px";
  buttonNext.style.borderRadius = "20px";
  buttonNext.style.fontWeight = "bold";

  buttonDiv.appendChild(buttonReplace);
  buttonDiv.appendChild(buttonNext);

  mainDivConfirm.appendChild(buttonDiv);

  // Adding onClick Event to the Camera Button
  cameraDiv.addEventListener("click", () => {
    clearInterval(context.__drawImage_Inteval);

    mainDivConfirm.style.display = "flex";
    maniDivControls.style.display = "none";

    // Pausing the Video
    const video_element = context.shadowRoot.querySelector("selfie-video");
    if (video_element) {
      video_element.pause();
    }
  });

  // Adding onClick Event to the Replace Button
  buttonReplace.addEventListener("click", () => {
    const videoElement = context.shadowRoot.getElementById("selfie-video");
    const canvasElement = context.shadowRoot.getElementById("selfie-canvas");
    if (videoElement) {
      if(context.__drawImage_Inteval){
        clearInterval(context.__drawImage_Inteval)
      }
      context.__drawImage_Inteval = setInterval(() => {
        __drawImage(context, videoElement, canvasElement);
        console.log("Through Replace Button");
      }, 100);

      // Hide the Button Div and Show the Camera Click Div
      mainDivConfirm.style.display = "none";
      maniDivControls.style.display = "flex";
    }
  });

  // Adding onClick Event to the Next Button
  buttonNext.addEventListener("click", () => {
    const parentDivForm = context.shadowRoot.querySelector("#parentDiv_Form");
    const selfieDiv = context.shadowRoot.querySelector("#parentDiv_Selfie");

    // Convert Canvas into Image
    const canvas_element = context.shadowRoot.querySelector("#selfie-canvas");
    context.photo_with_aayushmann.src = __canvasToImage(canvas_element);

    // Set the Background Image to the Form Div
    parentDivForm.style.backgroundImage =
      "url('" + context.photo_with_aayushmann.src + "')";
    parentDivForm.style.backgroundRepeat = "no-repeat";
    parentDivForm.style.backgroundSize = "cover";
    // Getting the Size of the backgroung Image
    context.photo_with_aayushmann.onload = ()=>{
      parentDivForm.style.height = `${context.photo_with_aayushmann.naturalHeight}px`;
      parentDivForm.style.width = `${context.photo_with_aayushmann.naturalWidth}px`;
    }

    // Show the Form Section
    parentDivForm.style.display = "flex";
    selfieDiv.style.display = "none";
  });
}

function __createFormSection(context) {
  const parentDivForm = context.shadowRoot.querySelector("#parentDiv_Form");

  // Section 1

  // Creating an Image with the background
  const mainDivClickedImage = document.createElement("div");
  mainDivClickedImage.setAttribute("id", "mainDiv_ClickedImage");
  mainDivClickedImage.style.background = "#000000b3";
  mainDivClickedImage.style.width = "fit-content";
  mainDivClickedImage.style.padding = "20px 10px";
  mainDivClickedImage.style.borderRadius = "10px";
  mainDivClickedImage.style.display = "flex";
  mainDivClickedImage.style.flexDirection = "column";
  mainDivClickedImage.style.gap = "20px";
  mainDivClickedImage.style.width = "80%";
  mainDivClickedImage.style.maxWidth = "360px";
  mainDivClickedImage.style.height = "fit-content";

  // Creating a Form and Heading
  const headingfortheForm = document.createElement("div");
  headingfortheForm.innerText =
    "Fill in the details for a personalised message from Ayushman Khurana";
  headingfortheForm.style.textAlign = "center";
  headingfortheForm.style.color = "white";
  headingfortheForm.style.fontWeight = "bold";
  headingfortheForm.style.fontFamily = "Poppins";

  // Name Input
  const nameInput = __createInputElement(context, "nameInput", "Name", "text", "name");
  // Email Input
  const emailInput = __createInputElement(
    context,
    "emailInput",
    "Email Id",
    "email", 
    "email"
  );
  // Mobile Number Input
  const phoneNumberInput = __createInputElement(
    context,
    "phoneNumberInput",
    "Mobile Number",
    "number",
    "mobile-number"
  );

    //Error Text
    const errorDiv = document.createElement('div')
    errorDiv.style.color = "red"
    errorDiv.style.fontWeight = "bold"
    errorDiv.style.fontSize = "14px"
    errorDiv.style.textAlign = "center"
    errorDiv.style.width = "230px"
    errorDiv.style.margin = "auto"


  // Submit Button Input
  const buttonDivSubmit = document.createElement("div");
  buttonDivSubmit.style.textAlign = "center";
  const submitButton = document.createElement("button");
  submitButton.setAttribute("id", "submitButton");
  submitButton.innerText = "Submit";
  submitButton.style.border = "none";
  submitButton.style.background = context.mainThemeColor;
  submitButton.style.padding = "10px 30px";
  submitButton.style.borderRadius = "20px";
  submitButton.style.fontWeight = "bold";
  buttonDivSubmit.appendChild(submitButton);

  // Appending all the Input fields to the MainDivClickedImage
  mainDivClickedImage.appendChild(headingfortheForm); //Append the Heading
  mainDivClickedImage.appendChild(nameInput);
  mainDivClickedImage.appendChild(emailInput);
  mainDivClickedImage.appendChild(phoneNumberInput);
  mainDivClickedImage.appendChild(errorDiv);
  mainDivClickedImage.appendChild(buttonDivSubmit);

  parentDivForm.appendChild(mainDivClickedImage);

  // Style Input Element
  __styleInput(mainDivClickedImage);

  //Submit Button Form 
  submitButton.addEventListener('click', ()=>{
    
    const allInputElement = context.shadowRoot.querySelectorAll('#mainDiv_ClickedImage input');


    let isValid = true;
    allInputElement.forEach(input=>{
        const name = input.name;
        const value = input.value;

        if(!value){
           isValid = false;
           errorDiv.style.display = "block"
           errorDiv.innerText = "Name, Email and Mobile Number are mandatory"
           
           return
        }
        if(name == "email" && !value.includes("@")){
            isValid = false
            errorDiv.style.display = "block"
            errorDiv.innerText = "Invalid Email ID"
            return
        }
        if(name === "mobile-number" && value.length !== 10){
            isValid = false;
            errorDiv.style.display = "block"
            errorDiv.innerText = "Mobile Number should be of 10 Digits"
            return
        }
        
       
    })

    if(isValid){
        errorDiv.style.display = "none"
        // console.log("Submittesd");

        let payload = {}

        allInputElement.forEach(input=>{
            if(input.name === "name"){
                payload["name"] = input.value
            }
            else if(input.name === "email"){
                payload["email"] = input.value
            }
            else {
                payload["mobile"] = input.value
            }
        })

        payload.image = context.photo_with_aayushmann.src;
        console.log(payload);


        fetch(`${context.api_hostName}${context.api_endPoint}`, {
            "method" : "POST",
            "body": JSON.stringify(payload),
            "headers": {
                'Content-type': 'application/json; charset=UTF-8',
                'Accept': 'application/json',
            }
        })
        .then(res=>{
            if(res.status === 201){
              gwd?.actions?.gwdGoogleAd?.goToPage("gwd-ad", "expanded-page_2")
            }
            else{
              errorDiv.style.display = "block";
              errorDiv.innerText = res?.error?.message
            }
        })
        .catch(err=>{
            console.log(error);
        })
    }

  })
}

async function startCamera(context, videoDiv, canvasDiv) {
  try {
    var constraints = {
      audio: false,
      video: {
          facingMode: 'user'
      }
 }

 navigator.mediaDevices.getUserMedia(constraints).then(function success(stream) {
  videoDiv.srcObject = stream;
 });

  } catch (err) {
    console.log("Error in accessing the camera ", err);
  }
}

function __drawImage(context, videoElement, canvasElement) {
  const ctx = canvasElement.getContext("2d");

  ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
  const video_width = videoElement.width;
  const video_height = videoElement.height;

  // canvasElement.width = video_width;
  // canvasElement.height = video_height;

  //   Fliping Video Element
  ctx.translate(video_width, 0);
  ctx.scale(-1, 1);
  ctx.drawImage(videoElement, 0, 0);

  //   Fliping Aayushmann
  ctx.translate(video_width, 0);
  ctx.scale(-1, 1);
  ctx.drawImage(context.aayushmann_Image, 0, 0);
}

function __canvasToImage(canvasElement) {
  return canvasElement.toDataURL();
}

function __styleInput(mainParentDivInput) {
  mainParentDivInput.querySelectorAll("input").forEach((input) => {
    input.style.backgroundColor = "white";
    input.style.padding = "10px";
    input.style.border = "none";
    input.style.borderRadius = "5px";
    input.style.fontWeight = "bold";
    input.style.fontFamily = "Poppins";
    input.style.width = "230px";
  });
}

function __createInputElement(context, id, placeholder, type, name) {
  const div = document.createElement("div");
  div.style.textAlign = "center";

  const Input = document.createElement("input");
  Input.setAttribute("id", `${type}Input`);
  Input.setAttribute("name", name)
  Input.type = type;
  Input.setAttribute("placeholder", placeholder);

  div.appendChild(Input);

  return div;
}

class GetSelfieClicked extends HTMLElement {
  constructor() {
    super();
    this.context = this;
    this.mainThemeColor = "#f1cc00";
    this.default_width = "355";
    this.default_height = window.innerHeight;
    this.default_canvas_height = "550"
    this.default_capture_height = "84"
    this.__drawImage_Inteval = null;
    this.aayushmann_Image = new Image();
    this.photo_with_aayushmann = new Image();
    this.api_hostName = "https://ayushmannbhavaforever.ekaleido.co/"
    this.api_endPoint = "api/submissions";
    this.aayushmann_Image.src = "./assets/2_1.png";
    // console.log("Connected");
    const shadowRoot = this.attachShadow({ mode: "open" });

    const mainDiv = document.createElement("main");
    mainDiv.setAttribute("id", "mainDiv_WebComp");
    mainDiv.style.width = "fit-content";
    mainDiv.style.position = "absolute"
    mainDiv.style.left = "50%"
    mainDiv.style.top = "5%"
    mainDiv.style.marginLeft = `-${this.default_width/2}px`
    // mainDiv.style.marginTop = `-${this.default_canvas_height/2}px`

    // Page 1------
    const parentDivSelfie = document.createElement("div");
    parentDivSelfie.setAttribute("id", "parentDiv_Selfie");
    // parentDivSelfie.style.width = "fit-content";

    // Webcam Component
    const mainDivSelfieComp = document.createElement("div");
    mainDivSelfieComp.setAttribute("id", "mainDiv_SelfieComp");
    mainDivSelfieComp.style.width = "fit-content";
    mainDivSelfieComp.style.position = "relative";

    // Controls Component
    const controlsDiv = document.createElement("div");
    controlsDiv.setAttribute("id", "mainDiv_controls");

    // Replace and Next Button
    const confirmSectionDiv = document.createElement("div");
    confirmSectionDiv.setAttribute("id", "mainDiv_ConfirmSection");

    // Appending to ParentSelfie Div
    parentDivSelfie.appendChild(mainDivSelfieComp);
    parentDivSelfie.appendChild(controlsDiv);
    parentDivSelfie.appendChild(confirmSectionDiv);

    // Page 2----
    const parentDivForm = document.createElement("div");
    parentDivForm.setAttribute("id", "parentDiv_Form");
    parentDivForm.style.width = "100%";
    parentDivForm.style.height = "100%";
    parentDivForm.style.display = "none";
    parentDivForm.style.justifyContent = "center";
    parentDivForm.style.alignItems = "center";

    // Appending to the Main DIv
    mainDiv.appendChild(parentDivSelfie);
    mainDiv.appendChild(parentDivForm);

    shadowRoot.appendChild(mainDiv);

    // Create a Video Tag and Canvas
    __createOrUpdateVideo(this);
    // Creating Capture Section
    __createCaptureSection(this);

    // Create a Form Section
    __createFormSection(this);
  }

  connectedCallback() {
  }

  // Define Allowed Attributes
  static get observedAttributes() {
    return ["component_width", "component_height", "start_camera"];
  }

  attributeChangedCallback(attrName, oldVal, newVal) {
    // console.log("attribute changed" && newVal !== "");
    if (attrName === "component_width" && newVal !== "") {
      // this.setAttribute("component_width", newVal)
      const componentHeight = this.getAttribute("component_height");
      __createOrUpdateVideo(this, newVal, componentHeight);
    }

    if (attrName === "component_height" && newVal !== "") {
      // this.setAttribute("component_height", newVal)
      const componentWidth = this.getAttribute("component_width");
      __createOrUpdateVideo(this, componentWidth, newVal);
    }

    if(attrName === "start_camera" && newVal === "true"){
      console.log("Started Camera");

      const videoElement = this.shadowRoot.querySelector('#mainDiv_SelfieComp video')
      const canvasElement = this.shadowRoot.querySelector('#mainDiv_SelfieComp canvas')

      startCamera(this, videoElement, canvasElement)
    }
  }
}

customElements.define("diwali-selfie", GetSelfieClicked);
